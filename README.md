## Installing Mods

1. Install r2modmanager https://thunderstore.io/package/ebkr/r2modman/
2. Start, choose Valheim as the game.
3. In the profile prompt -> Import/Update -> whatever -> From code `018fba9a-e6ea-ed8a-614c-8f81645d96a8`
    - Note! The code will change everytime the modlist is updated
4. Mods will appear in the list, wait and download
5. `Start modded` r2modman. Note! in the future, you must always start this way

## Default bindings

- `I` Interact with backpack
- `Left Alt` One button dodge
- `F1` Config manager
- `V` Attach/Deattach cart

## Mood music

https://open.spotify.com/playlist/0kmakdSy27LxkqiCV5VFbK?si=7b2ac2eb269542e5